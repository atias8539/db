#include "dataBaseAccess.h"
///////////////////////////////

int id = 0;
bool dataBaseAccess::open()
{
	std::string dbFileName = "galleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	if (doesFileExist)
	{
		////
	}
	return true;
}

void dataBaseAccess::close()
{
	sqlite3_close(db);
	db = nullptr;
}
void dataBaseAccess::printUsers()
{
	char* sqlStatement = "SELECT * FROM users;";
	char *errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement, callbackPrint, nullptr, &errMessage);
	if (res != SQLITE_OK) 
	{
		//expection
	}
}
int callbackPrint(void *data, int argc, char** argv, char**azColName)
{
	for (int i = 0; i < argc; i++)
	{
		std::cout << azColName[i] << " = " << argv[i] << " , "<<"\n";
	}
	return 0;
}
void dataBaseAccess::createUser(User& user)
{
	std::string name = user.getName();
	int id = getMaxId()+1;
	user.setId(id);

	std::string sqlStatement = "INSERT INTO USERS VALUES("+std::to_string(id)+",\""+name+"\");";
	char *errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException("unable to create user");
	}
	
}
int dataBaseAccess::getMaxId()
{
	std::string sql = "select max(id) from users;";
	char *errMessage = nullptr;
	int userId;
	int res = sqlite3_exec(db, sql.c_str(), callbackCount, &userId, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException("unable to find user");
	}
	else
	{
		return userId;
	}
}
int dataBaseAccess::getMaxIdAlbum()
{
	std::string sql = "select max(id) from albums;";
	char *errMessage = nullptr;
	int albumId;
	int res = sqlite3_exec(db, sql.c_str(), callbackCount, &albumId, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException("unable to find album");
	}
	else
	{
		return albumId;
	}
}
void dataBaseAccess::deleteUser(const User& user)
{
	std::string name = user.getName();
	int id = user.getId();
	std::string sqlStatement = "delete from USERS where id="+std::to_string(id)+";";
	char *errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException("unable to delete user");

	}
	
}
User dataBaseAccess::getUser(int userId)
{
	std::string sqlStatement = "SELECT NAME FROM USERS WHERE ID="+std::to_string(userId)+";";
	char *errMessage = nullptr;
	std::string name;
	User user();
	int res = sqlite3_exec(db, sqlStatement.c_str(), findUser, &name, &errMessage);
	if (res == SQLITE_OK)
	{
		return  User(userId,name);
	}
	else
	{
		throw MyException("unable to get user");
	}
}
int findUser(void *data, int argc, char** argv, char**azColName)
{
	std::string* c = (std::string*)data;
	*c =argv[0];
	return 0;
}
int dataBaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	std::string sqlStatement = "SELECT COUNT(*) FROM ALBUMS WHERE ALBUMS.USER_ID =" + std::to_string(user.getId()) + ";";
	char *errMessage = nullptr;
	int count;

	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount,&count, &errMessage);
	if (res == SQLITE_OK)
	{
		return count;
	}
	else
	{
		throw MyException("unable to count");
	}
}
int dataBaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	std::string sqlStatement = "select count( *) from tags where tags.user_id =" + std::to_string(user.getId()) + ";";
	char *errMessage = nullptr;
	int count;

	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &count, &errMessage);
	if (res == SQLITE_OK)
	{
		return count;
	}
	else
	{
		throw MyException("unable to count");
	}
}


int dataBaseAccess::countTagsOfUser(const User& user)
{
	std::string sqlStatement = "SELECT count(tags.PICTURE_ID) from tags WHERE TAGS.USER_ID=" + std::to_string(user.getId()) + ";";
	char *errMessage = nullptr;
	int count;

	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &count, &errMessage);
	if (res == SQLITE_OK)
	{
		return count;
	}
	else
	{
		throw MyException("unable to count");
	}
}

void dataBaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string sqlStatement = "delete from albums where user_id =" +std::to_string(userId)+" and name = \""+albumName+"\";";
	char *errMessage = nullptr;
	int count;

	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
	
		throw MyException("unable to delete album");
	}
}

void dataBaseAccess::printAlbums()
{
	std::string sqlStatement = "select * from albums;";
	char *errMessage = nullptr;
	int count;

	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackPrint, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException("unable to print albums");
	}
}



bool dataBaseAccess::doesUserExists(int userId)
{
	std::string sqlStatement = "select count (id) from users where id ="+std::to_string(userId)+";";
	char *errMessage = nullptr;
	int count;

	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &count, &errMessage);
	if (res == SQLITE_OK)
	{
		if (count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		throw MyException("unable to open albums");
	}
}


bool dataBaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	std::string sqlStatement = "select count(id) from albums where user_id =" + std::to_string(userId) + " and name = \""+albumName+"\";";
	char *errMessage = nullptr;
	int count;

	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &count, &errMessage);
	if (res == SQLITE_OK)
	{
		if (count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		throw MyException("unable to send sql querey");
	}
}
void dataBaseAccess::createAlbum(const Album& album)
{
	std::string sqlStatement = "select max(id) from albums;";
	char *errMessage = nullptr;
	int count;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &count, &errMessage);
	int userId = 0;
	char time[9];
	int albumId = 0;
	std::string name;
	if (res == SQLITE_OK)
	{
		albumId = count + 1;
		userId = album.getOwnerId();
		_strdate(time);
		name = album.getName();
		sqlStatement = "insert into albums  (ID,NAME,CREATION_DATE,USER_ID) values(" + std::to_string(albumId) + ",\"" + name.c_str() + "\"" +",\""+ time +"\"," + std::to_string(userId) +  ");";
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
		
			throw MyException("can't create albums");
		}
	}
	else
	{
		throw MyException("can't gets albums");

	}
}
void dataBaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	std::string sqlStatement = "select id from albums where name =\""+albumName+"\";";
	char *errMessage = nullptr;
	int albumId;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &albumId, &errMessage);
	char time[9];
	_strdate(time);
	std::string path=picture.getPath();
	std::string namePic=picture.getName();
	int picId = picture.getId();
	if (res == SQLITE_OK)
	{
		sqlStatement = "insert into pictures(ID,NAME,LOCATION,CREATION_DATE,ALBUM_ID) values(" + std::to_string(getIdPic()+1) + ",\"" + namePic.c_str() + "\",\"" + path.c_str() + "\",\"" + time + "\","+std::to_string(albumId)+");";
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			throw MyException("can't add picture to album");
		}
	}
	else
	{
		throw MyException("can't find this album");
	}
}
int dataBaseAccess::getIdPic()
{
	std::string sqlStatement = "select max(id) from pictures;";
	char *errMessage = nullptr;
	int picId=0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &picId, &errMessage);
	if (res == SQLITE_OK)
	{
		return picId;
	}
	throw MyException("cant find");
}
void dataBaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	std::string sqlStatement = "select id from albums where name =\"" + albumName + "\";";
	char *errMessage = nullptr;
	int albumId;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &albumId, &errMessage);
	if (res == SQLITE_OK)
	{
		sqlStatement = "delete from pictures where album_id=" + std::to_string(albumId) + " and name=\"" + pictureName.c_str() + "\";";
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			throw MyException("can't delete picture");
		}
	}
	else
	{
	throw MyException("can't find this album");
	}
}

void dataBaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string sqlStatement = "select id from albums where name =\"" + albumName + "\";";
	char *errMessage = nullptr;
	int albumId;
	int pictureId;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &albumId, &errMessage);
	if (res == SQLITE_OK)
	{
		sqlStatement = "select id from pictures where album_id=" + std::to_string(albumId) + " and name=\"" + pictureName.c_str() + "\";";
		res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &pictureId, &errMessage);
		if (res == SQLITE_OK)
		{
			sqlStatement = "INSERT INTO TAGS VALUES(" + std::to_string(pictureId) + "," + std::to_string(userId) + ");";
			res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

			if (res != SQLITE_OK)
			{
			
				throw MyException("cant insert tag");
			}
		}
		else
		{
			throw MyException("cant find this picture");
		}
		
	}
	else
	{
		throw MyException("can't find this album");
	}
}

void dataBaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	
	std::string sqlStatement = "select id from albums where name =\"" + albumName + "\";";
	char *errMessage = nullptr;
	int albumId;
	int pictureId;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &albumId, &errMessage);
	if (res == SQLITE_OK)
	{
		sqlStatement = "select id from pictures where album_id=" + std::to_string(albumId) + " and name=\"" + pictureName.c_str() + "\";";
		res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &pictureId, &errMessage);
		if (res == SQLITE_OK)
		{
			sqlStatement = "DELETE FROM TAGS WHERE PICTURE_ID=" + std::to_string(pictureId) + " AND USER_ID=" + std::to_string(userId) + ";";
			res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

			if (res != SQLITE_OK)
			{
			
				throw MyException("cant DELETE tag");
			}
		}
		else
		{
			throw MyException("cant find this picture");
		}

	}
	else
	{
		throw MyException("can't find this album");
	}
}

float dataBaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	float avgg = countAlbumsOwnedOfUser(user) / countAlbumsTaggedOfUser(user);
	return avgg;
}
int creUser(void *data, int argc, char **argv, char **azColName)
{
	User* user = (User*)data;
	for (int i = 0; i < argc; i++) 
	{
		if (std::string(azColName[i]) == "ID") {
			user->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			user->setName(argv[i]);
		}
	}
	return 0;
}
int callbackCount(void *data, int argc, char** argv, char**azColName)
{
	int* c = (int*)data;
	*c = atoi(argv[0]);
	return 0;
}

void dataBaseAccess::closeAlbum(Album &pAlbum)
{
	
	
}
void dataBaseAccess::cleanUserData(const User& userId)
{
	delete(&userId);
}

User dataBaseAccess::getTopTaggedUser()
{
	std::string sqlStatement = "SELECT users.id FROM tags INNER JOIN users ON users.id=tags.user_id GROUP BY users.id ORDER BY sum(users.id)/users.id  DESC limit 1;";
	char *errMessage = nullptr;
	int id=0;
	User user2(0,"");
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &id, &errMessage);
	if (res == SQLITE_OK)
	{
		sqlStatement = "select * from users where id=" + std::to_string(id); +";";
		int res = sqlite3_exec(db, sqlStatement.c_str(), creUser, &user2, &errMessage);
		if (res == SQLITE_OK)
		{
			return user2;
		}
		else
		{
			throw MyException("unable to find user");
		}
	}
	else
	{
		throw MyException("unable to find user album");
	}
	return user2;
}

int callbackAlbums(void *data, int argc, char** argv, char**azColName)
{
	Album album;
	for (int i = 0; i < argc; i++) {
		
		if (std::string(azColName[i]) == "NAME") {
			album.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			album.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID") {
			album.setOwner((int)argv[i]);
		}
		album.setId(id + 1);
	}
	std::list<Album>* ptr=(std::list<Album>*)data;
	ptr->push_back(album);
	
	return 0;
}
const std::list<Album> dataBaseAccess::getAlbums()
{
	std::string sqlStatement = "SELECT * from albums;";
	char *errMessage = nullptr;
	int id;
	std::list<Album> albums;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackAlbums, &albums, &errMessage);
	if (res == SQLITE_OK)
	{
		
			return albums;
	
	}
	else
	{
		throw MyException("unable to find albums");
	}
	return albums;
}
const std::list<Album> dataBaseAccess::getAlbumsOfUser(const User& user)
{
	int userid = user.getId();
	std::string sqlStatement = "SELECT * from albums where user_id="+std::to_string(userid)+";";
	char *errMessage = nullptr;
	int id;
	std::list<Album> albums;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackAlbums, &albums, &errMessage);
	if (res == SQLITE_OK)
	{

		return albums;

	}
	else
	{
		throw MyException("unable to find album");
	}
	return albums;
}


Picture dataBaseAccess::getTopTaggedPicture()
{
	std::string sqlStatement = "select pictures.id from tags inner join pictures on pictures.id = tags.picture_id group by pictures.id order by sum(pictures.id) / pictures.id desc limit 1;";
	char *errMessage = nullptr;
	int id;
	Picture picture;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackPicture, &picture, &errMessage);
	if (res == SQLITE_OK)
	{

		return picture;

	}
	else
	{
		throw MyException("unable to find album");
	}
	return picture;
}
int callbackPicture(void *data, int argc, char** argv, char**azColName)
{
	Picture picture(3,"aa");
	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "ID") {
			picture.setId((int)argv[i]);
		}
		else if (std::string(azColName[i]) == "NAME") {
			picture.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION") {
			picture.setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			picture.setCreationDate(argv[i]);
		}
		
	}

	data = &picture;
	return 0;
}
Album dataBaseAccess::openAlbum(const std::string& albumName)
{
	std::string sqlStatement = "SELECT id from albums where name=\"" + albumName + "\";";
	char *errMessage = nullptr;
	int id;
	Album album(0, "a");
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackCount, &id, &errMessage);
	if (res == SQLITE_OK)
	{
		sqlStatement = "select * from albums where id=" + std::to_string(id) + ";";
		res = sqlite3_exec(db, sqlStatement.c_str(), callbackAlbum, &album, &errMessage);
		if (res == SQLITE_OK)
		{
			album.setId(getMaxIdAlbum() + 1);
			std::string sqlStatement = "SELECT * from pictures where album_id=" + std::to_string(id) + ";";
			char *errMessage = nullptr;
			std::list < Picture> niv;
			int res = sqlite3_exec(db, sqlStatement.c_str(), callbackPictures, &niv, &errMessage);
			if (res == SQLITE_OK)
			{
				
				
				
				for (std::list<Picture>::iterator it=niv.begin();it!=niv.end();it++)
				{
					char *errMessage = nullptr;
					int picID = it->getId();
					std::string sqlStatement = "SELECT * from tags where picture_id=" + std::to_string(picID) + ";";
					int res = sqlite3_exec(db, sqlStatement.c_str(), callbackTags, &*it, &errMessage);

					if (res != SQLITE_OK)
					{
					
						throw MyException("no");
					}



				}
				for (const auto& al : niv) 
				{
					album.addPicture(al);
				}
				return album;
			}

		}
		else
		{
			throw MyException("unable to find albums");
		}
		return album;
	}
}
	int callbackAlbum(void *data, int argc, char** argv, char**azColName)
	{
		Album* album = (Album*)data;
		for (int i = 0; i < argc; i++) 
		{

			if (std::string(azColName[i]) == "NAME") 
			{
				album->setName(argv[i]);
			}
			else if (std::string(azColName[i]) == "CREATION_DATE") 
			{
				album->setCreationDate(argv[i]);
			}
			else if (std::string(azColName[i]) == "USER_ID") 
			{
				album->setOwner(atoi(argv[i]));
			}
		}

		return 0;

	}
int callbackPictures(void *data, int argc, char** argv, char**azColName)
{
	Picture picture(3, "aa");
	std::list<Picture>* ptr=(std::list<Picture>*)data;
	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "ID") {
			picture.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			picture.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION") {
			picture.setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			picture.setCreationDate(argv[i]);
		}

	}
	ptr->push_back(picture);
	return 0;
}
int callbackTags(void *data, int argc, char** argv, char**azColName)
{
	Picture * ptr= (Picture*)data;

	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "USER_ID") {
			ptr->tagUser(atoi(argv[i]));
		}

	}
		
		return 0;
}
std::list<Picture> dataBaseAccess::getTaggedPicturesOfUser(const User& user)
{
	int userId = user.getId();
	std::string sqlStatement = "select * from tags where user_id ="+std::to_string(userId)+";";
	char *errMessage = nullptr;
	std::list<Picture> ptr;

	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackPictures, &ptr, &errMessage);
	if (res == SQLITE_OK)
	{
			return ptr;
		

	}
	else
	{
		throw MyException("unable to find albums");
	}
	return ptr;

}
void dataBaseAccess::clear()
{
	delete db;
}