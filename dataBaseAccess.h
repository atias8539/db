#pragma once
#include <list>
#include "Album.h"
#include "User.h"
#include "IDataAccess.h"
#include <iostream> 
#include  <string>
#include <string.h>
#include "MyException.h"
#include <time.h>
#include "Picture.h"
int creUser(void *data, int argc, char **argv, char **azColName);
int callbackPrint(void *data, int argc, char** argv, char**azColName);
int findUser(void *data, int argc, char** argv, char**azColName);
int callbackCount(void *data, int argc, char** argv, char**azColName);
int callbackAlbums(void *data, int argc, char** argv, char**azColName);
int callbackPicture(void *data, int argc, char** argv, char**azColName);
int callbackPictures(void *data, int argc, char** argv, char**azColName);
int callbackTags(void *data, int argc, char** argv, char**azColName);

int callbackAlbum(void *data, int argc, char** argv, char**azColName);

class dataBaseAccess: public IDataAccess
{

public:
	dataBaseAccess() = default;
	virtual ~dataBaseAccess() = default;

	// album related
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	void deleteAlbum(const std::string& albumName, int userId) override;
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void closeAlbum(Album &pAlbum) override;
	void printAlbums() override;

	int getMaxId();
	int getIdPic();
	int getMaxIdAlbum();
	// picture related
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;

	// user related
	void printUsers() override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override;
	bool doesUserExists(int userId) override;
	User getUser(int userId) override;

	// user statistics
	int countAlbumsOwnedOfUser(const User& user) override;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override; 

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;  
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;//kal

	bool open() override;
	void close() override;
	void clear() override;//clear database

private:
	std::list<Album> m_albums;
	std::list<User> m_users;

	sqlite3* db;
	auto getAlbumIfExists(const std::string& albumName);//lo hose

	Album createDummyAlbum(const User& user);//lo hose
	void cleanUserData(const User& userId);
};